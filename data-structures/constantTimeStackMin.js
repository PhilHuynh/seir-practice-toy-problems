/**
 * Write a stack using your preferred instantiation pattern. Implement a min function
 * that returns the minimum value of all the elements in the stack in constant time.stack.

 * All of the functions in the Stack should run in constant time!
 *
 * var example = new Stack()
 *  example.push(4)
 *  example.push(3)
 *  example.min() === 3
 *  example.push(3)
 *  example.push(2)
 *  example.push(2)
 *  example.min() === 2
 */

/**
  * Stack Class
  */
 class Stack {
  constructor() {
    // TODO: Your code here
  }

// add an item to the top of the stack
  push(value) {
    // TODO: Your code here
  };

// remove an item from the top of the stack
  pop() {
    // TODO: Your code here
    }

// return the number of items in the stack
  size() {
    // TODO: Your code here
  };

// return the minimum value in the stack
  min() {
    // TODO: Your code here
  };
};

